#![allow(unused_variables)]
#![feature(in_band_lifetimes)]
#![allow(dead_code)]
extern crate csv;

use std::fs;
use serde::export::fmt::Debug;
use serde::{Serialize, Deserialize};
use serde_derive;


static DELIM: u8 = '\u{0001}' as u8;

fn main() {
    {
        let contents: Vec<ModelInfo> = match csv_reader(&String::from("resource/test.csv")) {
            Ok(contents) => contents,
            Err(error) => panic!(error.to_string())
        };
        println!("{:#?}", contents);
    }

     {
         let contents: Vec<ModelInfo> = match csv_reader_gen(&String::from("resource/test.csv")) {
             Ok(contents) => contents,
             Err(error) => panic!(error.to_string())
         };
         println!("{:#?}", contents);
     }
}

pub fn csv_reader_gen<'de, T: Debug + serde::Deserialize<'de>>(path: &String) -> Result<Vec<T>, Box<dyn std::error::Error>>
 {
     let mut vec: Vec<T> = Vec::new();
     let mut rdr = csv::ReaderBuilder::new().has_headers(false).delimiter(DELIM).from_path(path)?;
     for result in rdr.deserialize() {
         let record = result?;
         // println!("{:?}", &record);
         vec.push(record);
     }
     Ok(vec)
 }

pub fn csv_reader(path: &String) -> Result<Vec<ModelInfo>, Box<dyn std::error::Error>> {
    let mut vec: Vec<ModelInfo> = Vec::new();
    let mut rdr = csv::ReaderBuilder::new().has_headers(false).delimiter(DELIM).from_path(path)?;
    for result in rdr.deserialize() {
        let record: ModelInfo = result?;
        println!("{:#?}", &record);
        vec.push(record);
    }
    Ok(vec)
}

#[derive(Serialize, Deserialize, Debug)]
pub struct ModelInfo {
    pub as_of_dt: String,
    pub model_key: String,
    pub model_data: String,
    pub update_ts: String,
    pub model_feature: String,
    pub model_param_score: String,
    pub model_param_score_other: String,
    pub mkt_code: String,
}

